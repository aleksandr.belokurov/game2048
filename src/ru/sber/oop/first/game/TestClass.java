package ru.sber.oop.first.game;

import ru.sber.oop.first.game.core.Game2048;
import ru.sber.oop.first.game.core.board.Direction;
import ru.sber.oop.first.game.core.utils.GameHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestClass {
    public static void main(String[] args) {
        Game2048 game2048 = new Game2048();
        game2048.init();
        game2048.move(Direction.UP);
        List<Integer> list = new ArrayList<>();
        Integer[] integers = {2, null, 2, 2};
        list.addAll(Arrays.asList(integers));
        GameHelper helper = new GameHelper();
        list = helper.moveAndMergeEqual(list);
        System.out.println(list);
    }

}
