package ru.sber.oop.first.game.core;

import ru.sber.oop.first.game.core.board.Board;
import ru.sber.oop.first.game.core.board.Direction;
import ru.sber.oop.first.game.core.board.Key;
import ru.sber.oop.first.game.core.board.SquareBoard;
import ru.sber.oop.first.game.core.utils.GameHelper;
import ru.sber.oop.first.game.core.utils.NotEnoughSpace;

import java.util.*;

public class Game2048 implements Game{

    public static final int GAME_SIZE = 4;
    private final Board<Key, Integer> board = new SquareBoard<>(GAME_SIZE);

    private static final int[][] DIRECTIONS = new int[][]{{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    private final GameHelper gameHelper = new GameHelper();
    private final Random random = new Random();


    private boolean isAvailableSpace() {
        boolean result = true;
        List<Key> availableSpace = board.availableSpace();
        if (availableSpace.isEmpty()) {
            result = false;
        }
        return result;
    }

    private boolean isCellThatCanBeMerged() {
        for (int i = 0; i < board.getHeight(); i++) {
            for (int j = 0; j < board.getWeight(); j++) {
                if (isCellWithSameNumber(i, j)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isCellWithSameNumber(int row, int column) {
        Integer originalValue = board.getValue(board.getKey(row, column));
        Integer checkingValue;
        for (int [] derection : DIRECTIONS) {

                checkingValue = board.getValue(board.getKey(row + derection[0], column + derection[1]));
                if (checkingValue == null) {
                    break;
                }
                if (checkingValue.equals(originalValue)) {
                    return true;
                }
        }
        return false;
    }

    private void fillForEndGame() {
        int value;
        for (int i = 0; i < board.getHeight(); i++) {
            if (i % 2 == 0) {
                 value = 2;
            } else {
                value = 4;
            }
            for (int j = 0; j < board.getWeight(); j++) {
                board.addItem(board.getKey(i, j), value);
                if (value == 2) {
                    value = 4;
                } else {
                    value = 2;
                }
            }
        }
    }

    private void moveUp() {
        List<Key> listKey;
        for (int i = 0; i < board.getWeight(); i++) {
            listKey = board.getColumn(i);
            Collections.sort(listKey);
            moveAndMerge(listKey);
        }
    }

    private void moveBottom() {
        List<Key> listKey;
        for (int i = 0; i < board.getWeight(); i++) {
            listKey = board.getColumn(i);
            Collections.sort(listKey);
            Collections.reverse(listKey);
            moveAndMerge(listKey);
        }
    }

    private void moveRight() {
        List<Key> listKey;
        for (int i = 0; i < board.getHeight(); i++) {
            listKey = board.getRow(i);
            Collections.sort(listKey);
            Collections.reverse(listKey);
            moveAndMerge(listKey);
        }
    }

    private void moveLeft() {
        List<Key> listKey;
        for (int i = 0; i < board.getHeight(); i++) {
            listKey = board.getRow(i);
            Collections.sort(listKey);
            moveAndMerge(listKey);
        }
    }

    private void moveAndMerge(List<Key> listKey) {
        List<Integer> listValue = new ArrayList<>();
        for (Key key : listKey) {
            listValue.add(board.getValue(key));
        }
        listValue = gameHelper.moveAndMergeEqual(listValue);
        for (int j = 0; j < listKey.size(); j++) {
            board.addItem(listKey.get(j), listValue.get(j));
        }
    }

    @Override
    public void init() {
        List<Integer> integerList = new ArrayList<>(GAME_SIZE * GAME_SIZE);
        for (int i = 0; i < GAME_SIZE * GAME_SIZE ; i++) {
            integerList.add(null);
        }
        board.fillBoard(integerList);
        try {
            addItem();
            addItem();
        } catch (NotEnoughSpace notEnoughSpace) {

        }

    }

    @Override
    public boolean canMove() {
        boolean result;
        if (!isAvailableSpace()) {
            result = isCellThatCanBeMerged();
        } else {
            result = true;
        }
        return result;
    }



    @Override
    public boolean move(Direction direction) {
        switch (direction) {
            case UP:
                moveUp();
                break;
            case BOTTOM:
                moveBottom();
                break;
            case LEFT:
                moveLeft();
                break;
            case RIGHT:
                moveRight();
                break;
        }
        try {
            addItem();
        } catch (NotEnoughSpace notEnoughSpace) {

        }
        return canMove();
    }

    @Override
    public void addItem() throws NotEnoughSpace {
        double randomDouble =  random.nextDouble();
        int value;
        if (randomDouble < 0.9) {
            value = 2;
        } else {
            value = 4;
        }
        List<Key> availableSpace = board.availableSpace();
        int randomInt = 0;
        if (availableSpace.size() > 1) {
            int max = availableSpace.size() - 1;
            randomInt = random.nextInt(max);
        }
        if (availableSpace.size() == 0) {
            throw new NotEnoughSpace();
        }
        board.addItem(availableSpace.get(randomInt), value);
    }

    @Override
    public Board getGameBoard() {
        return board;
    }

    @Override
    public boolean hasWin() {
        return board.hasValue(64);
    }

    @Override
    public String toString() {
        return board.toString();
    }



}
