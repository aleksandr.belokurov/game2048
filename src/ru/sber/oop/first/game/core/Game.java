package ru.sber.oop.first.game.core;

import ru.sber.oop.first.game.core.board.Board;
import ru.sber.oop.first.game.core.board.Direction;
import ru.sber.oop.first.game.core.utils.NotEnoughSpace;

public interface Game {
    void init();
    boolean canMove();
    boolean move(Direction direction);
    void addItem() throws NotEnoughSpace;
    Board getGameBoard();
    boolean hasWin();

}
