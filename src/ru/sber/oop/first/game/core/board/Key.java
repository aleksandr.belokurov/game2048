package ru.sber.oop.first.game.core.board;

import java.util.Objects;

public class Key implements Comparable<Key> {
    private int i;
    private int j;

    public Key(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Key key = (Key) o;
        return i == key.i && j == key.j;
    }

    @Override
    public int hashCode() {
        return Objects.hash(i, j);
    }

    @Override
    public int compareTo(Key o) {
        int result = Integer.compare(i, o.getI());
        if (result == 0) {
            result = Integer.compare(j, o.getJ());
        }
        return result;
    }
}
