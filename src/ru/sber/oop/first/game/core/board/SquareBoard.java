package ru.sber.oop.first.game.core.board;

import javafx.beans.binding.StringBinding;
import ru.sber.oop.first.game.core.utils.NotEnoughSpace;

import java.util.*;

public class SquareBoard <V>  extends Board <Key, V>   {

    private int size;

    public SquareBoard(int size) {
        super(size, size);
        this.size = size;
    }

    @Override
    public void fillBoard(List<V> list) {
        if (list.size() > size * size) {
            throw new RuntimeException("Error init. list has many value");
        }
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                int numberOfList = (i * size) + j;
                getBoard().put(new Key(i, j), list.get(numberOfList));
            }
        }
    }

    public List<Key> availableSpace() {
        List<Key> spaceList = new ArrayList<>();
        for (Map.Entry<Key, V> entry : getBoard().entrySet()) {
            if (entry.getValue() == null) {
                spaceList.add(entry.getKey());
            }
        }
        return spaceList;
    }

    public void addItem(Key key, V value) {

        getBoard().put(key, value);
    }

    public Key getKey(int i, int j) {
        for (Key key : getBoard().keySet()) {
            if (key.getI() == i && key.getJ() == j) {
                return key;
            }
        }
        return null;
    }

    public V getValue(Key key) {
        for (Map.Entry<Key, V> entry : getBoard().entrySet()) {
            if (entry.getKey() == key) {
                return entry.getValue();
            }
        }
        return null;
    }

    public List<Key> getColumn(int j) {
        List<Key> keyList = new ArrayList<>();
        getBoard().forEach((k,v) -> {
            if (k.getJ() == j) {
                keyList.add(k);
            }
        });
        Collections.sort(keyList);
        return keyList;
    }

    public List<Key> getRow(int i) {
        List<Key> keyList = new ArrayList<>();
        getBoard().forEach((k,v) -> {
            if (k.getI() == i) {
                keyList.add(k);
            }
        });
        Collections.sort(keyList);
        return keyList;
    }

    public boolean hasValue(V value) {
        return getBoard().containsValue(value);
    }

    public List<V> getValues(List<Key> keys) {
        List<V> valuesList = new ArrayList<>();
        for (Key key : keys) {
            valuesList.add(getValue(key));
        }
        return valuesList;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < size; i++) {
            str.append("[ ");
            for (int j = 0; j < size; j++) {
                str.append(getValue(getKey(i, j))).append(", ");
            }
            str.append("] \n");

        }
        return str.toString();
    }
}
