package ru.sber.oop.first.game.core.board;

public enum Direction {
    RIGHT,
    LEFT,
    UP,
    BOTTOM
}
