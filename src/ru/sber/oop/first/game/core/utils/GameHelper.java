package ru.sber.oop.first.game.core.utils;

import java.util.ArrayList;
import java.util.List;

public class GameHelper {

    // transfer null at the end of list
    private List<Integer> moveNullAtEndList(List<Integer> listOriginal) {
        List<Integer> resultList = new ArrayList<>(listOriginal);
        while (!allNullAtEndList(resultList)) {
            resultList.remove(null);
            resultList.add(null);
        }
        return resultList;
    }

    // Check that everything null at the end of the list.
    private boolean allNullAtEndList(List<Integer> list) {
        boolean result = true;
        if (list.contains(null)) {
            for (int i = list.indexOf(null) + 1; i < list.size(); i++) {
                if (list.get(i) != null) {
                    result = false;
                }
            }
        }
        return result;
    }

    // combining identical number
    private List<Integer> mergeNumber(List<Integer> listOriginal) {
        List<Integer> list = new ArrayList<>(listOriginal);
        int listSize = list.size();
            for (int i = 0; i < listSize - 1; i++) {
                if (list.get(i + 1) == null) {
                    break;
                }
                if (list.get(i).equals(list.get(i + 1))) {
                    list.set(i, list.get(i) * 2);
                    list.remove(i + 1);
                    list.add(null);
                }
            }
        return list;
    }

    public List<Integer> moveAndMergeEqual(List<Integer> list) {
        List<Integer> resultList = new ArrayList<>(list);
        resultList = moveNullAtEndList(resultList);
        resultList = mergeNumber(resultList);
        return resultList;
    }
}
